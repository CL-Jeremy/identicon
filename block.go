// SPDX-License-Identifier: MIT

package identicon

import "image"

var (
	// 可以出现在中间的方块，满足轴对称和中心对称
	centerBlocks = []blockFunc{b0, b1, b2, b3, b19, b20}

	// 可以出现在侧边的方块，满足 Y 轴对称，或满足中心对称且沿 y = -x 对称
	edgeBlocks = []blockFunc{b0, b1, b2, b3, b19, b20, b5, b6, b7, b8, b11, b12, b13, b14}

	// 所有方块
	blocks = []blockFunc{b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b19, b20, b21}
)

// 所有 block 函数的类型
type blockFunc func(img *image.Paletted, x, y, size int, angle int)

// 将多边形 points 旋转 angle 个角度，然后输出到 img 上，起点为 x,y 坐标
//
// points 中的坐标是基于左上角是原点的坐标系。
func drawBlock(img *image.Paletted, x, y, size int, angle int, points []int) {
	if angle > 0 { // 0 角度不需要转换
		m := size / 2
		rotate(points, m, m, angle)
	}

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			if pointInPolygon(i, j, points) {
				img.SetColorIndex(x+i, y+j, 1)
			}
		}
	}
}

// 反色中间小方块
//
//  ----------
//  |########|
//  |##    ##|
//  |##    ##|
//  |########|
//  ----------
func b0(img *image.Paletted, x, y, size int, angle int) {
	for i := x; i < x+size; i++ {
		for j := y; j < y+size; j++ {
			img.SetColorIndex(i, j, 1)
		}
	}

	l := size / 4
	x = x + l
	y = y + l

	for i := x; i < x+2*l; i++ {
		for j := y; j < y+2*l; j++ {
			img.SetColorIndex(int(i), int(j), 0)
		}
	}
}

// 小菱形
//
//  ---------
//  |       |
//  |   #   |
//  |  ###  |
//  | ##### |
//  |  ###  |
//  |   #   |
//  |       |
//  ---------
func b1(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	mm := m / 2
	drawBlock(img, x, y, size, 0, []int{
		m, mm,
		3 * mm, m,
		m, 3 * mm,
		mm, m,
		m, mm,
	})
}

// 中间小方块
//  ----------
//  |        |
//  |  ####  |
//  |  ####  |
//  |        |
//  ----------
func b2(img *image.Paletted, x, y, size int, angle int) {
	l := size / 4
	x = x + l
	y = y + l

	for i := x; i < x+2*l; i++ {
		for j := y; j < y+2*l; j++ {
			img.SetColorIndex(i, j, 1)
		}
	}
}

// 大菱形
//
//  ---------
//  |   #   |
//  |  ###  |
//  | ##### |
//  |#######|
//  | ##### |
//  |  ###  |
//  |   #   |
//  ---------
func b3(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, 0, []int{
		m, 0,
		size, m,
		m, size,
		0, m,
		m, 0,
	})
}

// b4
//
//  -------
//  |#####|
//  |#### |
//  |###  |
//  |##   |
//  |#    |
//  |------
func b4(img *image.Paletted, x, y, size int, angle int) {
	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		size, 0,
		0, size,
		0, 0,
	})
}

// b5 三角形堆叠在小菱形上
//
//  ---------
//  |   #   |
//  |  ###  |
//  | ##### |
//  |#######|
//  |   #   |
//  |  ###  |
//  |   #   |
//  ---------
func b5(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		m, 0,
		size, m,
		0, m,
		m, 0,
	})

	mm := m / 2
	drawBlock(img, x, y, size, angle, []int{
		m, m,
		3 * mm, 3 * mm,
		m, size,
		mm, 3 * mm,
		m, m,
	})
}

// b6 鱼 1
//
//  ---------
//  |   #   |
//  |  ###  |
//  | #   # |
//  |### ###|
//  |   #   |
//  |  ###  |
//  |  # #  |
//  ---------
func b6(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	mm := m / 2
	drawBlock(img, x, y, size, angle, []int{
		0, m,
		m, 0,
		size, m,
		m, m,

		3 * mm, mm,
		mm, mm,
		m, m,

		3 * mm, 3 * mm,
		3 * mm, size,
		m, 3 * mm,
		mm, size,
		mm, 3 * mm,
		m, m,
		0, m,
	})
}

// b7 鱼 2
//
//  -----------
//  |#### ####|
//  |##     ##|
//  |         |
//  |#       #|
//  |##     ##|
//  |#       #|
//  -----------
func b7(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	mm := m / 2
	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		size, 0,
		size, size,
		3 * mm, 3 * mm,
		size, m,

		m, 0,
		0, m,
		mm, 3 * mm,
		0, size,
		0, 0,
	})

}

// b8 三个堆叠的三角形
//
//  -----------
//  |    #    |
//  |   ###   |
//  |  #####  |
//  |  #   #  |
//  | ### ### |
//  |#########|
//  -----------
func b8(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	mm := m / 2

	// 顶部三角形
	drawBlock(img, x, y, size, angle, []int{
		m, 0,
		3 * mm, m,
		mm, m,
		m, 0,
	})

	// 底下左边
	drawBlock(img, x, y, size, angle, []int{
		mm, m,
		m, size,
		0, size,
		mm, m,
	})

	// 底下右边
	drawBlock(img, x, y, size, angle, []int{
		3 * mm, m,
		size, size,
		m, size,
		3 * mm, m,
	})
}

// b9 斜靠的三角形
//
//  ---------
//  |#      |
//  | ####  |
//  |  #####|
//  |  #### |
//  |   #   |
//  ---------
func b9(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		size, m,
		m, size,
		0, 0,
	})
}

// b10
//
//  ----------
//  |    ####|
//  |    ### |
//  |    ##  |
//  |    #   |
//  |####    |
//  |###     |
//  |##      |
//  |#       |
//  ----------
func b10(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		m, 0,
		size, 0,
		m, m,
		m, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		0, m,
		m, m,
		0, size,
		0, m,
	})
}

// b11
//
//  ----------
//  |####    |
//  |####    |
//  |####    |
//  |    ####|
//  |    ####|
//  |    ####|
//  ----------
func b11(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		m, 0,
		m, m,
		0, m,
		0, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		m, m,
		size, m,
		size, size,
		m, size,
		m, m,
	})
}

// b12
//
//  -----------
//  |#########|
//  |  #####  |
//  |    #    |
//  |#########|
//  |  #####  |
//  |    #    |
//  -----------
func b12(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		size, 0,
		m, m,
		0, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		0, m,
		size, m,
		m, size,
		0, m,
	})
}

// b13
//
//  -----------
//  |#########|
//  |  #####  |
//  |    #    |
//  |  #####  |
//  |#########|
//  -----------
func b13(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		size, 0,
		m, m,
		0, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		m, m,
		size, size,
		0, size,
		m, m,
	})
}

// b14
//
//  ----------
//  |   #    |
//  | ###    |
//  |####    |
//  |    ####|
//  |    ### |
//  |    #   |
//  ----------
func b14(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		m, 0,
		m, m,
		0, m,
		m, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		m, m,
		size, m,
		m, size,
		m, m,
	})
}

// b15
//
//  ----------
//  |####    |
//  |###     |
//  |#       |
//  |    ####|
//  |    ### |
//  |    #   |
//  ----------
func b15(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		m, 0,
		0, m,
		0, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		m, m,
		size, m,
		m, size,
		m, m,
	})
}

// b16
//
//  ------------
//  |    #    #|
//  |   ##   ##|
//  |  ###  ###|
//  | ##   ##  |
//  |#    #    |
//  ------------
func b16(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	qm := m / 4
	drawBlock(img, x, y, size, angle, []int{
		m, 0,
		m, m + qm,
		0, size,
		m, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		size, 0,
		size, m + qm,
		m, size,
		size, 0,
	})
}

// b17
//
//  ----------
//  |#####   |
//  |###     |
//  |#       |
//  |      ##|
//  |      ##|
//  ----------
func b17(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2

	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		m, 0,
		0, m,
		0, 0,
	})

	quarter := size / 4
	drawBlock(img, x, y, size, angle, []int{
		size - quarter, size - quarter,
		size, size - quarter,
		size, size,
		size - quarter, size,
		size - quarter, size - quarter,
	})
}

// b18
//
//  ----------
//  |#####   |
//  |####    |
//  |###   ##|
//  |##  ####|
//  |# ######|
//  ----------
func b18(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2

	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		m, 0,
		0, size,
		size, m,
		size, size,
		0, size,
		0, 0,
	})
}

// b19
//
//  ----------
//  |########|
//  |###  ###|
//  |#      #|
//  |###  ###|
//  |########|
//  ----------
func b19(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2

	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		m, 0,
		0, m,
		0, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		m, 0,
		size, 0,
		size, m,
		m, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		size, m,
		size, size,
		m, size,
		size, m,
	})

	drawBlock(img, x, y, size, angle, []int{
		0, m,
		m, size,
		0, size,
		0, m,
	})
}

// b20
//
//  -----------
//  |#       #|
//  |###   ###|
//  | ### ### |
//  |  #   #  |
//  | ### ### |
//  |###   ###|
//  |#       #|
//  -----------
func b20(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	q := size / 4

	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		m, q,
		q, m,
		0, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		size, 0,
		m + q, m,
		m, q,
		size, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		size, size,
		m, m + q,
		q + m, m,
		size, size,
	})

	drawBlock(img, x, y, size, angle, []int{
		0, size,
		q, m,
		m, q + m,
		0, size,
	})
}

// b21
//
//  ----------
//  |########|
//  |##   ###|
//  |#      #|
//  |###   ##|
//  |########|
//  ----------
func b21(img *image.Paletted, x, y, size int, angle int) {
	m := size / 2
	q := size / 4

	drawBlock(img, x, y, size, angle, []int{
		0, 0,
		size, 0,
		0, q,
		0, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		q + m, 0,
		size, 0,
		size, size,
		q + m, 0,
	})

	drawBlock(img, x, y, size, angle, []int{
		size, q + m,
		size, size,
		0, size,
		size, q + m,
	})

	drawBlock(img, x, y, size, angle, []int{
		0, size,
		0, 0,
		q, size,
		0, size,
	})
}
