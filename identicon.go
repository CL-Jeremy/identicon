// SPDX-License-Identifier: MIT

package identicon

import (
	"crypto/md5"
	"fmt"
	"image"
	"image/color"
	"math/rand"
)

const (
	minSize       = 16 // 图片的最小尺寸
	maxForeColors = 32 // 在New()函数中可以指定的最大颜色数量
)

// Identicon 用于产生统一尺寸的头像
//
// 可以根据用户提供的数据，经过一定的算法，自动产生相应的图案和颜色。
type Identicon struct {
	foreColors []color.Color
	backColor  color.Color
	size       int
	rect       image.Rectangle
}

// New 声明一个 Identicon 实例
//
// size 表示整个头像的大小；
// back 表示前景色；
// fore 表示所有可能的前景色，会为每个图像随机挑选一个作为其前景色。
func New(size int, back color.Color, fore ...color.Color) (*Identicon, error) {
	if len(fore) == 0 || len(fore) > maxForeColors {
		return nil, fmt.Errorf("前景色数量必须介于[1]~[%d]之间，当前为[%d]", maxForeColors, len(fore))
	}

	if size < minSize {
		return nil, fmt.Errorf("参数 size 的值(%d)不能小于 %d", size, minSize)
	}

	return &Identicon{
		foreColors: fore,
		backColor:  back,
		size:       size,

		// 画布坐标从0开始，其长度应该是 size-1
		rect: image.Rect(0, 0, size, size),
	}, nil
}

// Make 根据 data 数据产生一张唯一性的头像图片
func (i *Identicon) Make(data []byte) image.Image {
	h := md5.New()
	h.Write(data)
	sum := h.Sum(nil)

	// 角方块
	vbIndex := int(sum[0]+sum[1]+sum[2]+sum[3]) % len(blocks)

	// 侧边方块
	ebIndex := int(sum[4]+sum[5]+sum[6]+sum[7]) % len(edgeBlocks)

	// 中间方块
	cbIndex := int(sum[8]+sum[9]+sum[10]+sum[11]) % len(centerBlocks)
	if ebIndex < len(centerBlocks) { // 选择和侧边不同的方块
		cbIndex = int(sum[8]+sum[9]+sum[10]+sum[11]) % (len(centerBlocks) - 1)
		if cbIndex >= ebIndex {
			cbIndex++
		}
	}

	// 旋转角度
	angle := int(sum[12]+sum[13]+sum[14]) % 4

	// 根据最后一个字段，获取前景颜色
	color := int(sum[15]) % len(i.foreColors)

	return i.render(cbIndex, vbIndex, ebIndex, angle, color)
}

// Rand 随机生成图案
func (i *Identicon) Rand(r *rand.Rand) image.Image {
	vbIndex := r.Intn(len(blocks))
	ebIndex := r.Intn(len(edgeBlocks))
	cbIndex := ebIndex
	for cbIndex == ebIndex {
		cbIndex = r.Intn(len(centerBlocks))
	}
	angle := r.Intn(4)
	color := r.Intn(len(i.foreColors))

	return i.render(cbIndex, vbIndex, ebIndex, angle, color)
}

func (i *Identicon) render(cbIndex, vbIndex, ebIndex, angle, foreColor int) image.Image {
	p := image.NewPaletted(i.rect, []color.Color{i.backColor, i.foreColors[foreColor]})
	drawBlocks(p, i.size, centerBlocks[cbIndex], blocks[vbIndex], edgeBlocks[ebIndex], angle)
	return p
}

// Make 根据 data 数据产生一张唯一性的头像图片
//
// size 头像的大小。
// back, fore头像的背景和前景色。
func Make(size int, back, fore color.Color, data []byte) (image.Image, error) {
	i, err := New(size, back, fore)
	if err != nil {
		return nil, err
	}
	return i.Make(data), nil
}

// 将九个方格都填上内容。
// p 为画板；
// cb、vb、eb 为中间 4 格，角上 4 格，边上 8 格的填充函数；
// angle 为 vb、eb 的起始旋转角度。
func drawBlocks(p *image.Paletted, size int, cb, vb, eb blockFunc, angle int) {
	blockSize := size / 4
	twoBlockSize, threeBlockSize := 2*blockSize, 3*blockSize

	next := func() int { // 求 angle 顺时针旋转 90 度后的值，但不会大于 3
		next := angle + 1
		if next > 3 {
			return 0
		}
		return next
	}

	cb(p, blockSize, blockSize, blockSize, 0)
	cb(p, twoBlockSize, blockSize, blockSize, 0)
	cb(p, twoBlockSize, twoBlockSize, blockSize, 0)
	cb(p, blockSize, twoBlockSize, blockSize, 0)

	vb(p, 0, 0, blockSize, angle)
	eb(p, blockSize, 0, blockSize, angle)
	eb(p, twoBlockSize, 0, blockSize, next())

	angle = next()
	vb(p, threeBlockSize, 0, blockSize, angle)
	eb(p, threeBlockSize, blockSize, blockSize, angle)
	eb(p, threeBlockSize, twoBlockSize, blockSize, next())

	angle = next()
	vb(p, threeBlockSize, threeBlockSize, blockSize, angle)
	eb(p, twoBlockSize, threeBlockSize, blockSize, angle)
	eb(p, blockSize, threeBlockSize, blockSize, next())

	angle = next()
	vb(p, 0, threeBlockSize, blockSize, angle)
	eb(p, 0, twoBlockSize, blockSize, angle)
	eb(p, 0, blockSize, blockSize, next())
}
